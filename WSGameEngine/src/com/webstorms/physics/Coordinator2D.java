package com.webstorms.physics;

import android.util.FloatMath;

public class Coordinator2D {

	// In real world
	public static final int DIRECTION_NORTH = 0;
	public static final int DIRECTION_EAST = 90;
	public static final int DIRECTION_SOUTH = 180;
	public static final int DIRECTION_WEST = 270;
	
	private float x;
	private float y;
	private int velocity;
	private int degrees;
	private float verticalVelocity;
	private float horizontalVelocity;
	
	public Coordinator2D() {
		
	}
	
	public Coordinator2D(int originalX, int originalY, int velocity, int degrees) {
		this.x = originalX;
		this.y = originalY;
		this.degrees = degrees;
		this.velocity = velocity;
		
		// Calculate horizontal velocity
		
		// sin (alpha) = opp/hyp
		// sin (degrees) = opp/this.velocity / * this.velocity
		// sin (degrees) * this.velocity = this.horizontalVelocity
		this.horizontalVelocity = (float) FloatMath.sin((float) Math.toRadians(degrees)) * velocity;
		
		// Calculate vertical velocity
	
		// cos (alpha) = adj/hyp
		// cos (degrees) = adj/this.velocity / * this.velocity
		// cos (degrees) * this.velocity = this.verticalVelocity
		this.verticalVelocity = (float) FloatMath.cos((float) Math.toRadians(degrees)) * velocity * -1; // 2D grid is different, P(0,0) is at top left corner of screen, hence the -1
		
	}
	
	public void setLocation(int newX, int newY) {
		this.x = newX;
		this.y = newY;
	
	}
	
	public void setVelocity(int velocity) {
		if(this.velocity != velocity) {
			this.velocity = velocity;
			this.horizontalVelocity = FloatMath.sin((float) Math.toRadians(degrees)) * velocity;
			this.verticalVelocity = FloatMath.cos((float) Math.toRadians(degrees)) * velocity * -1;
			
		}
		
	}
	
	public void setAngle(int degrees) {
		if(this.degrees != degrees) {
			this.degrees = degrees;
			this.horizontalVelocity = (float) FloatMath.sin((float) Math.toRadians(degrees)) * velocity;
			this.verticalVelocity = (float) FloatMath.cos((float) Math.toRadians(degrees)) * velocity * -1;
			
		}
		
	}
	
	public void update() {
		this.x += this.horizontalVelocity;
		this.y += this.verticalVelocity;
		
	}
	
	public int getAngle() {
		return this.degrees;
		
	}
	
	public int getX() {
		return (int) this.x;
		
	}
	
	public int getY() {
		return (int) this.y;
		
	}
	
	public float getHorizontalVelocity() {
		return horizontalVelocity;
		
	}
	
	public float getVerticalVelocity() {
		return verticalVelocity;
		
	}
	
	public int getVelocity() {
		return this.velocity;
		
	}
	
	
}
