package com.webstorms.framework;

public abstract class Screen extends WSObject {

	/*
	 * In the pause and dispose method, listeners are being disposed, check if they exist first!
	 * 
	 */
	
	public static final int INTRO_TRANSITION_UPDATE = 0;
	public static final int INTRO_TRANSITION_COMPLETED_UPDATE = 1;
	public static final int OUT_TRANSITION_UPDATE = 2;
	public static final int OUT_TRANSITION_COMPLETED_UPDATE = 3;
	
	private int transitionType;
	
	/**
	 * This class is the blueprint for any screen you create and use within the game. It handles the lower level things
	 * and works in a cooperative manner with the game engine.
	 * 
	 */
	
	public Screen(Game game) {
		super(game);
		load();
		this.transitionType = Screen.INTRO_TRANSITION_UPDATE;
		WSLog.e(Game.GAME_ENGINE_TAG, this, "New Screen has been created");
		
	}
	
	public void load() {
		
	}
	
	/**
	 * <b><i>public void setTransitionUpdateType(int transitionUpdateType)</i></b>
	 * <br>
	 * Since: API 1
	 * <br>
	 * <br>
	 * Set the transition update type of the screen.
	 *	
	 */
	
	public void setTransitionUpdateType(int transitionUpdateType) {
		this.transitionType = transitionUpdateType;
				
	}
	
	public abstract void introTransitionUpdate();
	
	public abstract void introTransitionCompletedUpdate();
	
	public abstract void outTransitionUpdate();
	
	public abstract void outTransitionCompletedUpdate();
	
	/**
	 * <b><i>public void update()</i></b>
	 * <br>
	 * Since: API 1
	 * <br>
	 * <br>
	 * Leave this method as it is and to add functionality to this screen extend following methods:
	 * <br>
	 * public void introTransitionUpdate()
	 * <br>
	 * public void introTransitionCompletedUpdate()
	 * <br>
	 * public void outTransitionUpdate()
	 * <br>
	 * public void outTransitionCompletedUpdate()
	 *	
	 */
	
	public void update() {
		if(this.transitionType == Screen.INTRO_TRANSITION_UPDATE) {
			this.introTransitionUpdate();
			
		}
		else if(this.transitionType == Screen.INTRO_TRANSITION_COMPLETED_UPDATE) {
			this.introTransitionCompletedUpdate();
			
		}
		else if(this.transitionType == Screen.OUT_TRANSITION_UPDATE) {
			this.outTransitionUpdate();
			
		}
		else if(this.transitionType == Screen.OUT_TRANSITION_COMPLETED_UPDATE) {
			this.outTransitionCompletedUpdate();
			
		}
		
	}
	
	/**
	 * <b><i>public void render()</i></b>
	 * <br>
	 * Since: API 1
	 * <br>
	 * <br>
	 * Override the method to set what you would like to render.
	 *	
	 */
	
	public abstract void render();
	
	/**
	 * <b><i>public void resume()</i></b>
	 * <br>
	 * Since: API 1
	 * <br>
	 * <br>
	 * Initialize all the sensory that should be used within this screen.
	 * This will be called when the activity resumes.
	 *	
	 */
	
	public void resume() {
		
	}
	
	/**
	 * <b><i>public void pause()</i></b>
	 * <br>
	 * Since: API 1
	 * <br>
	 * <br>
	 * This method will disable all sensory and will be called when the activity pauses.
	 *	
	 */
	
	public void pause() {
		this.getGame().getInput().useAccelerometer(false);
		this.getGame().getInput().useKeyboard(false);
		this.getGame().getInput().useTouchscreen(false);
		
		this.getGame().getInput().detachOnTouchListener();
		this.getGame().getInput().detachOnKeyListener();
		
	}
	
	/**
	 * <b><i>public void onDispose()</i></b>
	 * <br>
	 * Since: API 1
	 * <br>
	 * <br>
	 * This method will be called when the screen is to be disposed. You can do all your cleanups in here.
	 *	
	 */
	
	public void onDispose() {
		
	}
	
	
}
