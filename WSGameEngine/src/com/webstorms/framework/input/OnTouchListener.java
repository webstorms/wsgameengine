package com.webstorms.framework.input;

public interface OnTouchListener {

	public void onTouch(Integer x, Integer y);
	
}
