package com.webstorms.framework.input;

import com.webstorms.framework.Game;
import com.webstorms.framework.WSLog;
import com.webstorms.framework.WSObject;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;


public class Input extends WSObject implements SensorEventListener, OnTouchListener, OnKeyListener {
	
	/*
	 * Rename the onScreenTouchedDown method to something simpler
	 * 
	 */
	
	float accelX;
    float accelY;
    float accelZ;
    boolean isScreenTouched;
    boolean isKeyBoardTouched;
    Integer touchX;
    Integer touchY;
    Integer keyCode;
    View view;
    SensorManager manager;
    Sensor accelerometer;
    MotionEvent event;
	
    //
    com.webstorms.framework.input.OnTouchListener onTouchListener;
    com.webstorms.framework.input.OnKeyListener onKeyListener;
    
    /**
	 * This class provides you with an intuitive way to access the input options of a phone.
	 * 
	 */
    
    public Input(Game game, View view) {
		super(game);
    	keyCode = null;
    	this.view = view;
    	manager = (SensorManager) this.getGame().getSystemService(Context.SENSOR_SERVICE);
    	accelerometer = manager.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        
	}
    
    public void setOnTouchListener(com.webstorms.framework.input.OnTouchListener onTouchListener) {
    	this.onTouchListener = onTouchListener;
    	
    }
    
    public void setOnKeyListener(com.webstorms.framework.input.OnKeyListener onKeyListener) {
    	this.onKeyListener = onKeyListener;
    	
    }
    
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		accelX = event.values[0];
        accelY = event.values[1];
        accelZ = event.values[2];
        
	}

	public void refreshTouchListener() {
		if(this.onTouchListener != null) {
		//	this.onTouchListener.onTouch(this.touchX, this.touchY);
			
		}
		
	}
	
	@Override
	public boolean onTouch(View view, MotionEvent event) {
		if((event.getAction() != MotionEvent.ACTION_UP)) {			
			this.isScreenTouched = true;
			if((int)(event.getX()) > this.getGame().getWSScreen().getLeftLetterboxLength() && (int)(event.getX()) < this.getGame().getWSScreen().getInverseRightLetterboxLength()) {
				// Convert the touch hence the ability for a valid engagement with the UI
				// minus leftLetterBox will subtract from the screens display if its (+) and add if its (-)
				// This applies with the scaleProportionallyScreen and fillProportionallyScreen methods
				// NOTE: Has to be leftLetterBox and not rightLetterbox
				touchX = (int)((event.getX() - this.getGame().getWSScreen().getLeftLetterboxLength()) * this.getGame().getWSScreen().getScaleXFromRealToVirtual());
				
			}
			if((int)(event.getY()) > this.getGame().getWSScreen().getTopLetterboxLength() && (int)(event.getY()) < this.getGame().getWSScreen().getInverseBottumLetterboxLength()) {
	        		// Convert the touch hence the ability for a valid engagement with the UI
				// minus topLetterbox will subtract from the screens display if its (+) and add if its (-)
				// This applies with the scaleProportionallyScreen and fillProportionallyScreen methods
				// NOTE: Has to be topLetterbox and not bottumLetterbox
				touchY = (int)((event.getY() - this.getGame().getWSScreen().getTopLetterboxLength()) * this.getGame().getWSScreen().getScaleYFromRealToVirtual());
	        		
			}
			
			WSLog.d(Game.GAME_ENGINE_TAG, this, "Pressed down onto touchscreen X: " + touchX + " Y: " + touchY);

			
		}
		else {
			this.isScreenTouched = false;
        	this.touchX = null;
        	this.touchY = null;
        	WSLog.d(Game.GAME_ENGINE_TAG, this, "Released touchscreen");
        	
		}
        
		if(this.onTouchListener != null) {
			this.onTouchListener.onTouch(this.touchX, this.touchY);
			
		}
		
		return true;
	}
	
	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {		
		if(event.getAction() == KeyEvent.ACTION_DOWN) {
			this.isKeyBoardTouched = true;
			if(keyCode == 24 || keyCode == 25) { // Volume control buttons
				return false;
			}
			
			this.keyCode = keyCode;
			WSLog.d(Game.GAME_ENGINE_TAG, this, "Pressed down key: " + this.keyCode);
			
		}
		else if(event.getAction() == KeyEvent.ACTION_UP) {
			WSLog.d(Game.GAME_ENGINE_TAG, this, "Released key: " + this.keyCode);
			this.isKeyBoardTouched = false;
			this.keyCode = null;
        	
		}
		
		if(this.onKeyListener != null) {
			this.onKeyListener.onKey(this.keyCode);
			
		}
		
		return true;
	}
	
	/**
	 * <b><i>public float getAccelX()</i></b>
	 * <br>
	 * Since: API 1
	 * <br>
	 * <br>
	 * Retrieve the current accelerometer data of the x axis.
	 *
	 *@return 
	 * The current accelerometer data of the x axis.
	 *
	 */
	
	public float getAccelX() {
        return accelX;
    }

	/**
	 * <b><i>public float getAccelY()</i></b>
	 * <br>
	 * Since: API 1
	 * <br>
	 * <br>
	 * Retrieve the current accelerometer data of the y axis.
	 *
	 *@return 
	 * The current accelerometer data of the y axis.
	 *
	 */
	
    public float getAccelY() {
        return accelY;
    }

    /**
	 * <b><i>public float getAccelZ()</i></b>
	 * <br>
	 * Since: API 1
	 * <br>
	 * <br>
	 * Retrieve the current accelerometer data of the z axis.
	 *
	 *@return 
	 * The current accelerometer data of the z axis.
	 *
	 */
    
    public float getAccelZ() {
        return accelZ;
    }
   
    /**
	 * <b><i>public boolean isScreenTouchDown()</i></b>
	 * <br>
	 * Since: API 1
	 * <br>
	 * <br>
	 * Check if the touch screen is currently pressed.
	 *
	 *@return 
	 * If the touch screen is currently pressed.
	 *
	 */
    
    public boolean isScreenTouchedDown() {
        return this.isScreenTouched;
        
    }
    
    /**
	 * <b><i>public boolean isKeyboardTouchDown()</i></b>
	 * <br>
	 * Since: API 1
	 * <br>
	 * <br>
	 * Check if the keyboard is currently pressed.
	 *
	 *@return 
	 * If the keyboard is currently pressed.
	 *
	 */
    
    public boolean isKeyboardTouchedDown() {
        return this.isKeyBoardTouched;
    	
    }
    
    /**
	 * <b><i>public boolean isKeyPressed(Integer keyCode)</i></b>
	 * <br>
	 * Since: API 1
	 * <br>
	 * <br>
	 * Check if the provided key is pressed.
	 *
	 *@param 
	 *Provide the key you would like to check if its pressed.
	 *
	 *@return 
	 * If the provided key is currently pressed.
	 *
	 */
    
    public boolean isKeyPressed(Integer keyCode) {
    	if(this.keyCode == null) {
    		//gameEngineLog.d(classTAG, "Can't compare the requested key because no key is pressed");
    		return false;
    	}
    	else if(this.keyCode == keyCode) {
    	//	this.keyCode = null;
    		return true;
    	}
    	else {
    		return false;
    	}
    	
    }
    
    /**
	 * <b><i>public int getTouchX()</i></b>
	 * <br>
	 * Since: API 1
	 * <br>
	 * <br>
	 * Retrieve the x location of the touch on the touch screen.
	 *
	 *@return 
	 * The x location of the touch on the touch screen.
	 *
	 */
    
    public Integer getTouchX() {
    	return this.touchX;
    	
    }

    /**
	 * <b><i>public int getTouchY()</i></b>
	 * <br>
	 * Since: API 1
	 * <br>
	 * <br>
	 * Retrieve the y location of the touch on the touch screen.
	 *
	 *@return 
	 * The y location of the touch on the touch screen.
	 *
	 */
    
    public Integer getTouchY() {
    	return this.touchY;
    	
    }
    
    /**
	 * <b><i>public Integer getTouchedKey()</i></b>
	 * <br>
	 * Since: API 1
	 * <br>
	 * <br>
	 * Retrieve the touched key.
	 *
	 *@return 
	 * The touched key.
	 *
	 */
    
    public Integer getTouchedKey() {
    	return this.keyCode;
    	
    }
    
    /**
	 * <b><i>public void useAccelerometer(boolean use)</i></b>
	 * <br>
	 * Since: API 1
	 * <br>
	 * <br>
	 * Set if you would like to enable the use of the accelerometer.
	 * 
	 * @param use 
	 * <br>
	 * True will enable the use of the accelerometer.
	 * <br>
	 * False will disable the use of the accelerometer.
	 *	
	 */
    
    public void useAccelerometer(boolean use) {
    	if(use == true) {
    		manager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME);
    		WSLog.d(Game.GAME_ENGINE_TAG, this, "Accelerometer enabled");
    	}
    	else {
    		manager.unregisterListener(this, accelerometer);
    		WSLog.d(Game.GAME_ENGINE_TAG, this, "Accelerometer disabled");
    	}
    }
    
    /**
	 * <b><i>public void useTouchscreen(boolean use)</i></b>
	 * <br>
	 * Since: API 1
	 * <br>
	 * <br>
	 * Set if you would like to enable the use of the touch screen.
	 * 
	 * @param use 
	 * <br>
	 * True will enable the use of the touch screen.
	 * <br>
	 * False will disable the use of the touch screen.
	 *	
	 */
    
    public void useTouchscreen(boolean use) {
    	if(use == true) {
    		view.setOnTouchListener(this);
    		WSLog.d(Game.GAME_ENGINE_TAG, this, "Touchscreen enabled");
    		
    	}
    	else {
    		view.setOnTouchListener(null);
    		WSLog.d(Game.GAME_ENGINE_TAG, this, "Touchscreen disabled");
    		
    	}
    	
    }
    
    /**
	 * <b><i>public void useKeyboard(boolean use)</i></b>
	 * <br>
	 * Since: API 1
	 * <br>
	 * <br>
	 * Set if you would like to enable the use of the keyboard.
	 * 
	 * @param use 
	 * <br>
	 * True will enable the use of the keyboard.
	 * <br>
	 * False will disable the use of the keyboard.
	 *	
	 */
    
    public void useKeyboard(boolean use) {
    	if(use == true) {
    		view.setOnKeyListener(this);
    		WSLog.d(Game.GAME_ENGINE_TAG, this, "Keyboard enabled");
    	}
    	else {
    		view.setOnKeyListener(null);
    		WSLog.d(Game.GAME_ENGINE_TAG, this, "Keyboard disabled");
    	}
    	
    }
    
    public void detachOnTouchListener() {
    	this.onTouchListener = null;
    	
    }

    public void detachOnKeyListener() {
    	this.onKeyListener = null;
    	
    }
    
    
}
