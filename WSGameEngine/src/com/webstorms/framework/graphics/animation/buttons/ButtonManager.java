package com.webstorms.framework.graphics.animation.buttons;

public class ButtonManager {

	private static String clickedButton;
	
	public static void setClickedButton(String buttonName) {
		ButtonManager.clickedButton = buttonName;
		
	}
	
	public static boolean isClickedButton(VirtualButton button) {
		return ButtonManager.clickedButton.equals(button.getName());
		
	}
	
	public static boolean isClickedButton(PhysicalButton button) {
		return ButtonManager.clickedButton.equals(button.getName());
		
	}
	
	
}
