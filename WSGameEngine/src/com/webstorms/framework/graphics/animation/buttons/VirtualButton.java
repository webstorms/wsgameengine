package com.webstorms.framework.graphics.animation.buttons;

import android.graphics.Bitmap;
import android.graphics.Rect;

import com.webstorms.framework.Game;
import com.webstorms.framework.graphics.animation.GraphicObject;

public class VirtualButton extends GraphicObject {
	
	private String name;
	private boolean isPressed;
	private Bitmap buttonTouched;
	private VirtualButtonListener listener;
	
	public VirtualButton(Game game, String name, int x, int y, Rect dimensions, Bitmap normal, Bitmap touched, VirtualButtonListener listener) {
		super(game, normal);
		this.name = name;
		this.getShape().addRectangle(dimensions);
		this.setLocation(x, y);
		this.buttonTouched = touched;
		this.listener = listener;
		
	}
	
	@Override
	public void defineShape() {
		
	}
	
	public String getName() {
		return this.name;
		
	}
	
	public boolean isPressed() {
		return this.isPressed;
		
	}
	
	public void update(Integer touchX, Integer touchY) {
		if(touchX != null && touchY != null) {			
			if(this.getShape().contains(touchX, touchY)) {
				this.isPressed = true;
				
			}
			else {
				this.isPressed = false;
				
			}
		}
		else if(this.isPressed == true) {
			this.isPressed = false;
			ButtonManager.setClickedButton(this.name);
			this.listener.onVirtualButtonClicked(this.name);
			
		}
		
	}
	
	public void render() {
		if(!this.isPressed) {
			this.renderBitmap();
			
		}
		else {
			this.getGame().getGraphics().drawBitmap(buttonTouched, null, this.getRect(), null);
			
		}
		
	}
	
	
}
