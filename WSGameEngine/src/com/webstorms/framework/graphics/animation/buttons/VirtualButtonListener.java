package com.webstorms.framework.graphics.animation.buttons;

public interface VirtualButtonListener {

	public void onVirtualButtonClicked(String buttonName);
	
}
