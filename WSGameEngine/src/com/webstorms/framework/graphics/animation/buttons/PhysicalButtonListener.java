package com.webstorms.framework.graphics.animation.buttons;

public interface PhysicalButtonListener {

	public void onPhysicalButtonClicked(String buttonName);
	
}
