package com.webstorms.framework.graphics.animation.buttons;

import com.webstorms.framework.Game;
import com.webstorms.framework.WSObject;
import com.webstorms.framework.util.Util;

public final class PhysicalButton extends WSObject {
	
	private String name;
	private boolean isPressed;
	int keyCode;
	PhysicalButtonListener listener;
	
	public PhysicalButton(Game game, String name, int keyCode, PhysicalButtonListener listener) {
		super(game);
		this.name = name;
		this.keyCode = keyCode;
		this.listener = listener;
	}
	
	public String getName() {
		return this.name;
		
	}
	
	public boolean isPressed() {
		return this.isPressed;
		
	}
	
	public void update(Integer touchKey) {
		if(touchKey != null) {
			if(Util.isKeyPressed(touchKey, this.keyCode)) {
				this.isPressed = true;
				
			}
			else {
				this.isPressed = false;
				
			}	
			
		}
		else if(this.isPressed == true) {
			this.isPressed = false;
			ButtonManager.setClickedButton(this.name);
			this.listener.onPhysicalButtonClicked(this.name);
			
		}
		
	}
	
	
}
