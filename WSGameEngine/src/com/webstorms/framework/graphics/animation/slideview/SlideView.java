package com.webstorms.framework.graphics.animation.slideview;

import android.graphics.Bitmap;
import android.graphics.Rect;

import com.webstorms.framework.Game;
import com.webstorms.framework.graphics.animation.GraphicObject;

public class SlideView extends GraphicObject {
	
	/* Last edit: 22 July 2012
	 * 
	 * Improvements: 
	 * - Create a fixed amount of visible boards, to reduce updating and rendeing time, yielding better FPS
	 * - Add setGravity(int gravity) // in % (allow the boards to keep on moving for a little bit after having been dragged)
	 * - Don't keep on checking closest board to rest position in the updateUnactive() method
	 * - public void moveBoardToPosition(int pos); and public void updateUnactive() have very similiar code, maybe compromise! 
	 * 
	 * Bugs:
	 * - None (that I know of) :D
	 */
	
	public static final int TYPE_ACTIVE = 0;
	public static final int TYPE_UNACTIVE = 1;
	
	private Integer xStartingPosition;
	private int xSlideVelocity;
	private Integer indexOfBoardClosestToRest;
	private int shortestDistanceToRest;
	
	SlideViewOptions options;
	
	private Integer slideViewFirstBoardPosition;
	private Integer slideViewLastBoardPosition;
	
	public SlideView(Game game, SlideViewOptions options, Rect dimensions, Bitmap bitmap) {
		super(game, bitmap);
		this.options = options;
		this.initializeSlideView();
		this.getShape().addRectangle(new Rect(dimensions));
		
	} 
	
	@Override
	public void defineShape() {
		
	}
	
	private void initializeSlideView() {
		if(this.options.getSuppliedBoards().size() != 0) {
			this.slideViewFirstBoardPosition = 1;
			slideViewLastBoardPosition = this.options.getSuppliedBoards().size();
			int boardLength = this.options.getSuppliedBoards().get(0).getShape().getWidth();
			
			// Set up positions of the boards
			for(int amountOfBoards = 0; amountOfBoards < this.options.getSuppliedBoards().size(); amountOfBoards++) {
				if(amountOfBoards == 0) {
					this.options.getSuppliedBoards().get(amountOfBoards).setX(this.options.getRestPosition()); // Use the restPosition as a reffernce
				
				}
				else {
					this.options.getSuppliedBoards().get(amountOfBoards).setX(this.options.getSuppliedBoards().get(amountOfBoards - 1).getX() + boardLength + this.options.getDistanceOfGapBetweenBoards());
					
				}
				
			}
			
			// Move all boards to the desired start position
			int distanceToMove = this.options.getSuppliedBoards().get(0).getX() - this.options.getSuppliedBoards().get(this.options.getStartPosition() - 1).getX(); // -1 To convert from position to index
			this.updateSlideObjects(distanceToMove);
	 		
			// Set the index of the board closest to the rest position, to prevent jerking, because the index is defaulty 0
			this.indexOfBoardClosestToRest = this.options.getStartPosition() - 1; // convert index to position, hence -1
			
		}
		else {
			this.slideViewFirstBoardPosition = null;
			this.slideViewLastBoardPosition = null;
			this.indexOfBoardClosestToRest = null;
			
		}
		
	}
	
	// Setter methods
	
	public void setSlideViewInfo(SlideViewOptions options) {
		this.options = options;
		this.initializeSlideView();
		
	}
	
	// Getter methods
	
	public SlideViewOptions getSlideViewInfo() {
		return this.options;
		
	}
	
	public boolean isRestBoardFirst() {
		if(this.getPositionOfBoardClosestToRestPosition() == this.slideViewFirstBoardPosition) {
			return true;
			
		}
		else {
			return false;
			
		}
		
	}
	
	public boolean isRestBoardLast() {
		if(this.getPositionOfBoardClosestToRestPosition() == this.slideViewLastBoardPosition) {
			return true;
			
		}
		else {
			return false;
			
		}
		
	}
	
	public Integer getIndexOfBoardClosestToRestPosition() {
		return this.indexOfBoardClosestToRest;
		
	}
	
	public Integer getPositionOfBoardClosestToRestPosition() {
		if(this.indexOfBoardClosestToRest != null) {
			return this.indexOfBoardClosestToRest + 1;
			
		}
		else {
			return null;
					
		}
		
	}
	
	// Functional methods
	
	@Override
	public void render() {
		super.render();
		for(int amountOfBoards = 0; amountOfBoards < this.options.getSuppliedBoards().size(); amountOfBoards++) {
			this.options.getSuppliedBoards().get(amountOfBoards).render();
			
		}
		
	}
	
	public void update(int type, Integer xLocationOfTouch) {
		if(type == SlideView.TYPE_ACTIVE) {
			if(xLocationOfTouch != null) {
				this.updateActive(xLocationOfTouch);
				
			}
			else {
				this.updateActive(0);
				
			}
			
		}
		else if(type == SlideView.TYPE_UNACTIVE) {
			this.updateUnactive();
			
		}
		
	}

	public void updateActive(int touchX) {
		// Move all boards according to finger-movement
		if(xStartingPosition == null) {
			xStartingPosition = touchX;
		}
		this.xSlideVelocity = touchX - this.xStartingPosition;
		this.updateSlideObjects(this.xSlideVelocity);
		this.xStartingPosition = touchX;
		
		// If newIndex is not null, this means that the moving was interuppted with the physical interaction
		if(this.newIndex != null) {
			this.newIndex = null;
		}
		
	}
	
	Integer newIndex = null;
	
	public boolean moveBoardToPosition(int pos) {
		// Intialize the newIndex, which all boards will shift accordingly with
		if(this.newIndex == null && pos > 0 && pos <= this.options.getSuppliedBoards().size()) {
		//	this.getGameEngineLog().d(classTAG, "Setting new index!");
			this.newIndex = pos - 1; // Convert pos to index
		}
		else {
		//	this.getGameEngineLog().d(classTAG, "Not setting new index!");
			
		}
		
		// If newIndex couldn't be set, since it wasn't a valid argument, return true (meaning the slideView has finsished sliding)
		if(this.newIndex == null) {
			return true;
		}
		
		// Move all Boards back into position
		if(this.options.getRestPosition() > this.options.getSuppliedBoards().get(this.newIndex).getX()) {
			// Move right
			// Fine movement
			if(this.options.getRestPosition() - this.options.getSuppliedBoards().get(this.newIndex).getX() < this.options.getxSlideVelocityUnActive()) {
				this.xSlideVelocity = this.options.getRestPosition() - this.options.getSuppliedBoards().get(this.newIndex).getX();
				this.updateSlideObjects(this.xSlideVelocity); // Has to be +
				
			}
			// Coarse movement
			else {
				this.xSlideVelocity = this.options.getxSlideVelocityUnActive();
				this.updateSlideObjects(this.xSlideVelocity);
			}
						
		}
		else {
			// Move left
			if(this.options.getSuppliedBoards().get(this.newIndex).getX() - this.options.getRestPosition() < this.options.getxSlideVelocityUnActive()) {
				this.xSlideVelocity = this.options.getRestPosition() - this.options.getSuppliedBoards().get(this.newIndex).getX(); // Has to be -
				this.updateSlideObjects(this.xSlideVelocity);
			}
			else {
				this.xSlideVelocity = - this.options.getxSlideVelocityUnActive();
				this.updateSlideObjects(this.xSlideVelocity);
			}
			
		}
		
		if(this.options.getSuppliedBoards().get(this.newIndex).getX() == this.options.getRestPosition()) {
			this.newIndex = null;
			return true;
			
		}
		else {
			return false;
			
		}
		
	}
	
	public void updateUnactive() {
		this.xStartingPosition = null;
		if(this.options.getSuppliedBoards().size() != 0) {
			// Move all Boards back into position
			if(this.options.getRestPosition() > this.options.getSuppliedBoards().get(this.indexOfBoardClosestToRest).getX()) {
				// Move right
				// Fine movement
				if(this.options.getRestPosition() - this.options.getSuppliedBoards().get(this.indexOfBoardClosestToRest).getX() < this.options.getxSlideVelocityUnActive()) {
					this.xSlideVelocity = this.options.getRestPosition() - this.options.getSuppliedBoards().get(this.indexOfBoardClosestToRest).getX();
					this.updateSlideObjects(this.xSlideVelocity); // Has to be +
					
				}
				// Coarse movement
				else {
					this.xSlideVelocity = this.options.getxSlideVelocityUnActive();
					this.updateSlideObjects(this.xSlideVelocity);
				}
					
			}
			else {
				// Move left
				if(this.options.getSuppliedBoards().get(this.indexOfBoardClosestToRest).getX() - this.options.getRestPosition() < this.options.getxSlideVelocityUnActive()) {
					this.xSlideVelocity = this.options.getRestPosition() - this.options.getSuppliedBoards().get(this.indexOfBoardClosestToRest).getX(); // Has to be -
					this.updateSlideObjects(this.xSlideVelocity);
				}
				else {
					this.xSlideVelocity = - this.options.getxSlideVelocityUnActive();
					this.updateSlideObjects(this.xSlideVelocity);
				}
				
			}
			
		}
		
	}
	
	public void updateSlideObjects(int velocity) {
		
		if(this.options.getSuppliedBoards().size() != 0) {
			this.indexOfBoardClosestToRest = 0; // First Board object
			this.shortestDistanceToRest = this.options.getRestPosition() - this.options.getSuppliedBoards().get(0).getX(); // First Board object
			
		}
		
		for(int amountOfBoards = 0; amountOfBoards < this.options.getSuppliedBoards().size(); amountOfBoards++) {
			this.updateSingleSlideObject(amountOfBoards, velocity);
		
		}
		
	}
	
	public void updateSingleSlideObject(int index, int velocity) {
		// Move
		this.options.getSuppliedBoards().get(index).update(velocity);
		
		// Set the refference for the rest board
		if(this.options.getRestPosition() > this.options.getSuppliedBoards().get(index).getX()) {
			if(this.options.getRestPosition() - this.options.getSuppliedBoards().get(index).getX() < this.shortestDistanceToRest) {
				this.indexOfBoardClosestToRest = index;
				this.shortestDistanceToRest = this.options.getRestPosition() - this.options.getSuppliedBoards().get(index).getX();
				
			}
		}
		else {
			if(this.options.getSuppliedBoards().get(index).getX() - this.options.getRestPosition() < this.shortestDistanceToRest) {
				this.indexOfBoardClosestToRest = index;
				this.shortestDistanceToRest = this.options.getSuppliedBoards().get(index).getX() - this.options.getRestPosition();
			}
			
		}
		
	}
	
	
}
