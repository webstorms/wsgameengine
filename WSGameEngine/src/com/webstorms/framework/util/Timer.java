package com.webstorms.framework.util;

import android.util.Log;

public class Timer extends Object {

	public Timer() {
		
	}
	
	public Timer(int delayTime) {
		this.delayTime = delayTime;
		
	}
	
	private Long startTime;
	private int delayTime;
	
	
	
	public void setDelayTime(int delayTime) {
		this.delayTime = delayTime;
		
	}
	
	public boolean delay() {
		if(this.startTime == null) {
			this.startTime = System.currentTimeMillis();
		}
		
		if(startTime + this.delayTime < System.currentTimeMillis()) {
		//	this.startTime = null;
			return true;
			
		}
		else {
			return false;
			
		}
		
	}
	
	// The same as resume, just renew the startTime
	
	public void reset() {
		this.startTime = System.currentTimeMillis();
		
	}
	
	public void pause() {
		// Check if timer is still running
		if(this.delay() == false) {
			// Calculate new delayTime
			delayTime = (int) ((startTime + this.delayTime) - System.currentTimeMillis());
			Log.d("SpaceDroid", "Timer: Pause, new delay time: " + delayTime);
			
		}
		
		
	}
	
	
}
