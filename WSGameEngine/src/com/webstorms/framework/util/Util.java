package com.webstorms.framework.util;

import java.util.Random;

public class Util {
	
	/**
	 * <b><i>public static int centerObject(int objectLength, int start, int end)</i></b>
	 * <br>
	 * Since: API 1
	 * <br>
	 * <br>
	 * Get the x value of centering a object between to points.
	 * 
	 * @return 
	 * The x value that centers the object between to points.
	 *
	 */
	
	public static int centerObject(int objectLength, int start, int end) {
		return ((end - start) - objectLength)/2 + start;
		
	}
	
	public static int getRandomNumberBetween(int min, int max) {
        Random foo = new Random();
        int randomNumber = foo.nextInt(max - min) + min;
        if(randomNumber == min) {
            // Since the random number is between the min and max values, simply add 1
            return min + 1;
        }
        else {
            return randomNumber;
        }

    }
	
	public static int getRandomNumberFrom(int min, int max) {
        Random foo = new Random();
        int randomNumber = foo.nextInt((max + 1) - min) + min;
        
        return randomNumber;

    }
	
	public static boolean getRandomBoolean() {
        Random foo = new Random();
        return foo.nextBoolean();

    }
	
	public static boolean inRangeBetween(int start, int end, int value) {
		return (value > start && value < end);
		
	}
	
	public static boolean inRangeFrom(int start, int end, int value) {
		return ((value > start || value == start) && (value < end || value == end));
	}
	
	public static float round(float value, int decimalPlaces) {
		float p = (float) Math.pow(10,decimalPlaces);
		value *= p;
		float tmp = Math.round(value);
		return (float)tmp/p;
		
		
	}
	
	public static int getAbsoluteAngle(int degrees) {
		int absoluteValue;
		
		if(degrees > 0) {
			absoluteValue = degrees;
			
		}
		else {
			absoluteValue = 360 + degrees;
			
		}
		
		return absoluteValue;
		
	}
	
	/**
	 * <b><i>public boolean isKeyPressed(Integer keyCode1, Integer keyCode2)</i></b>
	 * <br>
	 * Since: API 1
	 * <br>
	 * <br>
	 * Check if the provided key is pressed.
	 *
	 *@param 
	 *Provide the key you would like to check if its pressed and the key to compare to.
	 *
	 *@return 
	 * If the provided key is currently pressed.
	 *
	 */
    
    public static boolean isKeyPressed(Integer keyCode1, Integer keyCode2) {
    	if(keyCode1.equals(keyCode2) == true) {
    		return true;
    		
    	}
    	else {
    		return false;
    		
    	}
    }
	
}
