package com.webstorms.framework.audio;

import com.webstorms.framework.Game;
import com.webstorms.framework.WSObject;
import com.webstorms.framework.WSLog;

import android.media.SoundPool;

public class Sound extends WSObject {

	int soundId;
    SoundPool soundPool;
    int volumeLeft = 0;
	int volumeRight = 0;
	boolean isPlaying;
	
	public Sound(Game game, SoundPool soundPool, int soundId) {
		super(game);
		this.soundId = soundId;
        this.soundPool = soundPool;
        isPlaying = false;
        this.setVolume(100); // Default
		
	}
	
	public void play() {
		soundPool.play(soundId, volumeLeft, volumeRight, 0, 0, 1);
		isPlaying = true;
		
	}
	
	public void setVolume(int vL, int vR) {
		volumeLeft = vL/100;
		volumeRight = vR/100;
		soundPool.setVolume(soundId, volumeLeft, volumeRight);
		
	}
	
	public void setVolume(int volume) {
		volumeLeft = volume/100;
		volumeRight = volume/100;
		soundPool.setVolume(soundId, volumeLeft, volumeRight);
		
	}
	
	public void pause() {
		soundPool.pause(soundId);
		isPlaying = false;
		
	}
	
	public void unload() {
		soundPool.unload(soundId);
		
	}

	public boolean isPlaying() {
		return isPlaying;
		
	}
	
	
}
