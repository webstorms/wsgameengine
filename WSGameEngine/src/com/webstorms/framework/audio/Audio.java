package com.webstorms.framework.audio;

import java.io.IOException;

import com.webstorms.framework.Game;
import com.webstorms.framework.WSObject;
import com.webstorms.framework.WSLog;

import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;

public class Audio extends WSObject {
	
	/**
	 * Factory class that generates Music and Sound instances.
	 * 
	 */
	
	public Audio(Game game) {
		super(game);
		this.getGame().setVolumeControlStream(AudioManager.STREAM_MUSIC);
		
	}
	
	/**
	 * <b><i>public Music newMusic(String filename)</i></b>
	 * <br>
	 * Since: API 1
	 * <br>
	 * <br>
	 * Retrieve a music instance.
	 * 
	 * @param filename
	 * The name of the music file (preferable ".mp3").
	 * 
	 * @return
	 * A Music instance.
	 *
	 */
	
	public Music newMusic(String filename) {
		AssetFileDescriptor assetDescriptor = null;
			try {
				assetDescriptor = this.getGame().getIO().readAssetsAudio(filename);
			} catch (IOException e) {
			}
		return new Music(this.getGame(), new MediaPlayer(), assetDescriptor);
		
	}
	
	/**
	 * <b><i>public Sound newSound(String filename)</i></b>
	 * <br>
	 * Since: API 1
	 * <br>
	 * <br>
	 * Retrieve a sound instance.
	 * 
	 * @param filename
	 * The name of the sound file (preferable ".ogg").
	 * 
	 * @return
	 * A Sound instance.
	 *
	 */
	
	public Sound newSound(String filename) {
		AssetFileDescriptor assetDescriptor = null;
		SoundPool soundPool = new SoundPool(20, AudioManager.STREAM_MUSIC, 0);
		int soundId = 0;
			try {
				assetDescriptor = this.getGame().getIO().readAssetsAudio(filename);
			} catch (IOException e) {
			}
			soundId = soundPool.load(assetDescriptor, 1);
		return new Sound(this.getGame(), soundPool, soundId);
	}
	
	
}
